<?php

/**
 * @file
 * Account UI.
 */
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\user\Entity\Role as RoleEntity;

/**
 * Implements hook_help().
 */
function account_ui_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.account_ui':
      return 'Account UI.';
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for user form.
 */
function account_ui_form_user_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  foreach ($form['account'] as $name => $element) {
    if (!is_array($element) || !array_key_exists('#type', $element)) {
      continue;
    }
    if (array_key_exists($name, $form)) {
      continue;
    }
    $form[$name] = $element;
    unset($form['account'][$name]);
  }

  $config = \Drupal::config('account_ui.settings');
  if ($config->get('ignore_permissions')) {
    foreach ($form as $name => $element) {
      if (is_array($element) && array_key_exists('#access', $element)) {
        $form[$name]['#access'] = true;
      }
    }
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function account_ui_entity_extra_field_info() {
  $fields = array();
  $fields['user']['user']['form'] = array();
  $form = &$fields['user']['user']['form'];
  $display = &$fields['user']['user']['display'];

  $form['current_pass'] = array(
    'label' => t('Current password'),
    'weight' => -10
  );

  $form['mail'] = array(
    'label' => t('Email address'),
    'weight' => -9
  );
  $display['mail'] = array(
    'label' => t('Email address'),
    'weight' => -9
  );

  $form['name'] = array(
    'label' => t('Username'),
    'weight' => -8
  );
  $display['name'] = array(
    'label' => t('Username'),
    'weight' => -8
  );

  $form['pass'] = array(
    'label' => t('Change the current password'),
    'weight' => -7
  );

  $form['status'] = array(
    'label' => t('Status'),
    'weight' => -6
  );
  $display['status'] = array(
    'label' => t('Status'),
    'weight' => -6
  );

  $form['roles'] = array(
    'label' => t('Roles'),
    'weight' => -5
  );
  $display['roles'] = array(
    'label' => t('Roles'),
    'weight' => -5
  );

  $form['notify'] = array(
    'label' => t('Notify user of new account'),
    'weight' => -4
  );

  $form['signature_settings'] = array(
    'label' => t('Signature settings'),
    'weight' => -3
  );

  return $fields;
}

/**
 * Implements hook_entity_extra_field_info_alter().
 */
function account_ui_entity_extra_field_info_alter(&$info) {
  unset($info['user']['user']['form']['account']);
}

/**
 * Implements hook_ENTITY_TYPE_view() for user entities.
 */
function account_ui_user_view(array &$build, UserInterface $account, EntityViewDisplayInterface $display) {
  $components = array();
  foreach (account_ui_entity_extra_field_info()['user']['user']['display'] as $name => $component) {
    $components[$name] = $component['label'];
  }
  unset($components['roles']);

  $config = \Drupal::config('account_ui.settings');
  $label_display = $config->get('label_display');

  foreach ($components as $name => $title) {
    if ($display->getComponent($name)) {
      $build[$name] = $account->getFields()[$name]->view();
      $build[$name]['#title'] = $title;
      $build[$name]['#label_display'] = $label_display;
    }
  }

  if ($display->getComponent('roles')) {
    $roles = array();
    foreach (RoleEntity::loadMultiple($account->getRoles()) as $role) {
      $roles[] = $role->label();
    }
    $build['roles'] = $account->getFields()['roles']->view();
    $build['roles']['#theme'] = 'item_list';
    $build['roles']['#title'] = t('Roles');
    $build['roles']['#items'] = $roles;
  }
}
